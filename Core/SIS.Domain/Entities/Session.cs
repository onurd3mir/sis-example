﻿using SIS.Domain.Entities.Common;

namespace SIS.Domain.Entities
{
    public class Session : BaseEntity
    {
        public int? ExpertEmployeeId { get; set; }
        public string? StartDate { get; set; }
        public string? EndDate { get; set; }
        public string? SessionNote { get; set; }
        public int? CancelEmployeeId { get; set; }
        public string? CancelNote { get; set; }
        public bool SessionStatus { get; set; }
        public bool CustomerCancelSession { get; set; }
        public int? CustomerId { get; set; }
        public Customer? Customer { get; set; }
        public Employee? Employee { get; set; }
    }
}
