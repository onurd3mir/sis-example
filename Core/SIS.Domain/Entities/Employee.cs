﻿using SIS.Domain.Entities.Common;

namespace SIS.Domain.Entities
{
    public class Employee : BaseEntity
    {
        public Employee()
        {
            Sessions = new HashSet<Session>();
        }

        public string? FullName { get; set; }
        public string? IdentityNumber { get; set; }
        public string? Email { get; set; }
        public string? Password { get; set; }
        public string? PhoneNumber { get; set; }
        public DateTime? BirthDate { get; set; }
        public bool? Gender { get; set; }
        public string? Address { get; set; }
        public short? EmployeeType { get; set; }
        public ICollection<Session> Sessions { get; set; }
    }
}
