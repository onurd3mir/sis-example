﻿namespace SIS.Application.Dtos
{
    public class ServiceResponseDto
    {
        public bool Result { get; set; }
        public string? Message { get; set; }
    }

    public class ServiceResponseDto<T> : ServiceResponseDto where T : class
    {
        public T? Data { get; set; }
    }
}
