﻿namespace SIS.Application.Dtos.Employees
{
    public class EmployeeDto
    {
        public int EmployeeId { get; set; }
        public string? FullName { get; set; }
        public string? IdentityNumber { get; set; }
        public string? Email { get; set; }
        public string? Password { get; set; }
        public string? PhoneNumber { get; set; }
        public DateTime? BirthDate { get; set; }
        public bool? Gender { get; set; }
        public string? Address { get; set; }
        public short? EmployeeType { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
