﻿namespace SIS.Application.Dtos.Employees
{
    public class EmployeeQueryDto
    {
        public string? FullName { get; set; }
        public short? EmployeeType { get; set; }
    }
}
