﻿namespace SIS.Application.Dtos.Employees
{
    public class EmployeeAuthorizeDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
