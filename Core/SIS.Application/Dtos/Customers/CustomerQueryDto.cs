﻿namespace SIS.Application.Dtos.Customers
{
    public class CustomerQueryDto
    {
        public string? FullName { get; set; }
        public string? IdentityNumber { get; set; }
    }
}
