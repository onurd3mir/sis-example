﻿namespace SIS.Application.Dtos.Sessions
{
    public class SessionDto
    {
        public int? SessionId { get; set; }
        public int? CustomerId { get; set; }
        public string? CustomerFullName { get; set; }
        public int? ExpertEmployeeId { get; set; }
        public string? ExpertEmployeFullName { get; set; }
        public DateTime? CreateDate { get; set; }
        public string? StartDate { get; set; }
        public string? EndDate { get; set; }
        public string? SessionNote { get; set; }
        public string? CancelNote { get; set; }
        public bool? SessionStatus { get; set; }
    }
}
