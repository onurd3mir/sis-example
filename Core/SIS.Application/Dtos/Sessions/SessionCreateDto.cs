﻿namespace SIS.Application.Dtos.Sessions
{
    public class SessionCreateDto
    {
        public int ExpertEmployeeId { get; set; }
        public string? StartDate { get; set; }
        public string? EndDate { get; set; }
    }
}
