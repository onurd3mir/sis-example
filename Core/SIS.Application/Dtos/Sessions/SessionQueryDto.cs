﻿namespace SIS.Application.Dtos.Sessions
{
    public class SessionQueryDto
    {
        public int? CustomerId { get; set; }
        public int? ExpertEmployeeId { get; set; }
        public bool SessionStatus { get; set; }
        public DateTime CreateDate { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}
