﻿namespace SIS.Application.Dtos.Sessions
{
    public class SessionMeetingSaveDto
    {
        public int SessionId { get; set; }
        public int CustomerId { get; set; }
    }
}
