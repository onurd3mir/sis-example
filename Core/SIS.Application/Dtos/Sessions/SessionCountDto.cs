﻿namespace SIS.Application.Dtos.Sessions
{
    public class SessionCountDto
    {
        public int SessionCount { get; set; }
    }
}
