﻿namespace SIS.Application.Dtos.Sessions
{
    public class SessionCancelDto
    {
        public int SessionId { get; set; }
        public int CancelEmployeeId { get; set; }
        public string? SessionNote { get; set; }
        public bool CancelCustomer { get; set; }
    }
}
