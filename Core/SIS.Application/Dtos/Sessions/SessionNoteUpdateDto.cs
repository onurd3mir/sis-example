﻿namespace SIS.Application.Dtos.Sessions
{
    public class SessionNoteUpdateDto
    {
        public int SessionId { get; set; }
        public string SessionNote { get; set; }
    }
}
