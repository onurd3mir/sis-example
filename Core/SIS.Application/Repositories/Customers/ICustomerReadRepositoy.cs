﻿using SIS.Domain.Entities;

namespace SIS.Application.Repositories.Customers
{
    public interface ICustomerReadRepositoy : IReadRepository<Customer>
    {
    }
}
