﻿using SIS.Domain.Entities;

namespace SIS.Application.Repositories.Customers
{
    public interface ICustomerWriteRepository : IWriteRepository<Customer>
    {
    }
}
