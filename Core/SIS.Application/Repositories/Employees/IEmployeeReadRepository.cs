﻿using SIS.Domain.Entities;

namespace SIS.Application.Repositories.Employees
{
    public interface IEmployeeReadRepository : IReadRepository<Employee>
    {
        Task<Employee> EmployeeAuthorizeAsync(string email, string password);
    }
}
