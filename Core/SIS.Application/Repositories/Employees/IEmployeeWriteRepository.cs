﻿using SIS.Domain.Entities;

namespace SIS.Application.Repositories.Employees
{
    public interface IEmployeeWriteRepository : IWriteRepository<Employee>
    {
    }
}
