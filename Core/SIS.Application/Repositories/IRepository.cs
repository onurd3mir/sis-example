﻿using Microsoft.EntityFrameworkCore;
using SIS.Domain.Entities.Common;

namespace SIS.Application.Repositories
{
    public interface IRepository<T> where T : BaseEntity
    {
        DbSet<T> Table { get; }
    }
}
