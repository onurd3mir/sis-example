﻿using SIS.Domain.Entities.Common;

namespace SIS.Application.Repositories
{
    public interface IWriteRepository<T> : IRepository<T> where T : BaseEntity
    {
        Task<bool> AddAsync(T model);
        Task<bool> AddRangeAsync(List<T> data);
        bool Remove(T model);
        Task<bool> RemoveAsync(int id);
        bool RemoveRange(List<T> data);
        bool Update(T model);
        Task<int> SaveAsync();
    }
}
