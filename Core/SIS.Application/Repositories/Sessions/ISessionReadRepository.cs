﻿using SIS.Application.Dtos.Sessions;
using SIS.Domain.Entities;

namespace SIS.Application.Repositories.Sessions
{
    public interface ISessionReadRepository : IReadRepository<Session>
    {
        List<SessionDto> GetSesionQuery(SessionQueryDto sessionQueryDto);
        SessionDto GetCustomerEndMeetingInfo(int customerId);
        SessionDto GetCustomerEndSessionInfo(int customerId);
    }
}
