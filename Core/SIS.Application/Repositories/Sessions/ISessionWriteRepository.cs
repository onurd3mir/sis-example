﻿using SIS.Domain.Entities;

namespace SIS.Application.Repositories.Sessions
{
    public interface ISessionWriteRepository : IWriteRepository<Session>
    {
    }
}
