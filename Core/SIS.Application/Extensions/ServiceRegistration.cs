﻿using Microsoft.Extensions.DependencyInjection;
using SIS.Application.Services;
using SIS.Application.Services.Interface;

namespace SIS.Application.Extensions
{
    public static class ServiceRegistration
    {
        public static void LoadApplicationServices(this IServiceCollection services)
        {
            services.AddScoped<IEmployeeService, EmployeeManager>();
            services.AddScoped<ICustomerService, CustomerManager>();
            services.AddScoped<ISessionService, SessionManager>();

            services.AddAutoMapper(typeof(Mapping.AutoMapper.ApplicationMappingProfile));
        }
    }
}
