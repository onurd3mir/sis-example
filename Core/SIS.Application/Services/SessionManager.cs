﻿using AutoMapper;
using SIS.Application.Dtos;
using SIS.Application.Dtos.Sessions;
using SIS.Application.Repositories.Sessions;
using SIS.Application.Services.Interface;
using SIS.Domain.Entities;

namespace SIS.Application.Services
{
    public class SessionManager : ISessionService
    {
        private readonly IMapper _mapper;
        private readonly ISessionReadRepository _sessionReadRepository;
        private readonly ISessionWriteRepository _sessionWriteRepository;

        public SessionManager(IMapper mapper, ISessionReadRepository sessionReadRepository, ISessionWriteRepository sessionWriteRepository)
        {
            _mapper = mapper;
            _sessionReadRepository = sessionReadRepository;
            _sessionWriteRepository = sessionWriteRepository;
        }

        public ServiceResponseDto<SessionCountDto> ExistSessionCount(SessionQueryDto sessionQueryDto)
        {
            var result = _sessionReadRepository.GetWhere(x =>
                x.ExpertEmployeeId == sessionQueryDto.ExpertEmployeeId &&
                x.CreateDate == sessionQueryDto.CreateDate &&
                x.StartDate == sessionQueryDto.StartDate &&
                x.EndDate == sessionQueryDto.EndDate, true).ToList();

            return new ServiceResponseDto<SessionCountDto>
            {
                Data = new SessionCountDto { SessionCount = result.Count },
                Result = true,
                Message = "Başarılı"
            };
        }

        public ServiceResponseDto<SessionDto> GetCustomerEndMeetingInfo(int customerId)
        {
            var result = _sessionReadRepository.GetCustomerEndMeetingInfo(customerId);
            return new ServiceResponseDto<SessionDto>
            {
                Data = result,
                Result = true,
                Message = "Başarılı"
            };
        }

        public ServiceResponseDto<SessionDto> GetCustomerEndSessionInfo(int customerId)
        {
            return new ServiceResponseDto<SessionDto>
            {
                Data = _sessionReadRepository.GetCustomerEndSessionInfo(customerId),
                Result = true,
                Message = "Başarılı"
            };
        }

        public ServiceResponseDto<List<SessionDto>> GetSesionQuery(SessionQueryDto sessionQueryDto)
        {
            var result = _sessionReadRepository.GetSesionQuery(sessionQueryDto);

            return new ServiceResponseDto<List<SessionDto>>
            {
                Data = result,
                Result = true,
                Message = "Başarılı"
            };
        }

        public async Task<ServiceResponseDto> SessionAddAsync(SessionCreateDto sessionCreateDto)
        {
            var entity = _mapper.Map<Session>(sessionCreateDto);
            await _sessionWriteRepository.AddAsync(entity);
            var result = await _sessionWriteRepository.SaveAsync() == 1;

            return new ServiceResponseDto
            {
                Result = result,
                Message = result == true ? "Başarılı" : "Başarısız"
            };
        }

        public async Task<ServiceResponseDto> SessionCancelAsync(SessionCancelDto sessionCancelDto)
        {
            var session = await GetSessionById(sessionCancelDto.SessionId);
            session.SessionNote = sessionCancelDto.SessionNote;
            session.CancelEmployeeId = sessionCancelDto.CancelEmployeeId;
            session.CustomerCancelSession = sessionCancelDto.CancelCustomer;
            _sessionWriteRepository.Update(session);
            var result = await _sessionWriteRepository.SaveAsync() == 1;

            return new ServiceResponseDto
            {
                Result = result,
                Message = result == true ? "Başarılı" : "Başarısız"
            };
        }

        public async Task<ServiceResponseDto> SessionMeetingCancelAsync(int sessionId)
        {
            var session = await GetSessionById(sessionId);
            session.CustomerId = null;
            _sessionWriteRepository.Update(session);
            var result = await _sessionWriteRepository.SaveAsync() == 1;

            return new ServiceResponseDto
            {
                Result = result,
                Message = result == true ? "Başarılı" : "Başarısız"
            };
        }

        public async Task<ServiceResponseDto> SessionMeetingSaveAsync(SessionMeetingSaveDto sessionMeetingSaveDto)
        {
            var session = await GetSessionById(sessionMeetingSaveDto.SessionId);
            session.CustomerId = sessionMeetingSaveDto.CustomerId;
            _sessionWriteRepository.Update(session);
            var result = await _sessionWriteRepository.SaveAsync() == 1;

            return new ServiceResponseDto
            {
                Result = result,
                Message = result == true ? "Başarılı" : "Başarısız"
            };
        }

        public async Task<ServiceResponseDto> SessionNoteUpdateAsync(SessionNoteUpdateDto sessionNoteUpdateDto)
        {
            var session = await GetSessionById(sessionNoteUpdateDto.SessionId);
            session.SessionNote = sessionNoteUpdateDto.SessionNote;
            _sessionWriteRepository.Update(session);
            var result = await _sessionWriteRepository.SaveAsync() == 1;

            return new ServiceResponseDto
            {
                Result = result,
                Message = result == true ? "Başarılı" : "Başarısız"
            };
        }

        private async Task<Session> GetSessionById(int id)
        {
            return await _sessionReadRepository.GetByIdAsync(id);
        }
    }
}
