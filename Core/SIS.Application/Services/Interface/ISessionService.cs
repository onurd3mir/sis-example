﻿using SIS.Application.Dtos;
using SIS.Application.Dtos.Sessions;

namespace SIS.Application.Services.Interface
{
    public interface ISessionService
    {
        Task<ServiceResponseDto> SessionAddAsync(SessionCreateDto sessionCreateDto);
        ServiceResponseDto<List<SessionDto>> GetSesionQuery(SessionQueryDto sessionQueryDto);
        ServiceResponseDto<SessionDto> GetCustomerEndMeetingInfo(int customerId);
        Task<ServiceResponseDto> SessionNoteUpdateAsync(SessionNoteUpdateDto sessionNoteUpdateDto);
        Task<ServiceResponseDto> SessionMeetingSaveAsync(SessionMeetingSaveDto sessionMeetingSaveDto);
        Task<ServiceResponseDto> SessionMeetingCancelAsync(int sessionId);
        Task<ServiceResponseDto> SessionCancelAsync(SessionCancelDto sessionCancelDto);
        ServiceResponseDto<SessionCountDto> ExistSessionCount(SessionQueryDto sessionQueryDto);
        ServiceResponseDto<SessionDto> GetCustomerEndSessionInfo(int customerId);
    }
}
