﻿using SIS.Application.Dtos;
using SIS.Application.Dtos.Employees;

namespace SIS.Application.Services.Interface
{
    public interface IEmployeeService
    {
        Task<ServiceResponseDto> EmployeeAddAsync(EmployeeCreateDto employeeInputDto);
        ServiceResponseDto<List<EmployeeDto>> GetEmployeeQuery(EmployeeQueryDto employeeQueryDto);
        ServiceResponseDto<List<EmployeeDto>> GetEmployeeAll();
        Task<ServiceResponseDto> EmployeeUpdateAsync(EmployeeUpdateDto employeeUpdateDto);
        Task<ServiceResponseDto<EmployeeDto>> GetEmployeeById(int id);
        Task<ServiceResponseDto<EmployeeDto>> EmployeeAuthorizeAsync(EmployeeAuthorizeDto employeeAuthorizeDto);
    }
}
