﻿using SIS.Application.Dtos;
using SIS.Application.Dtos.Customers;

namespace SIS.Application.Services.Interface
{
    public interface ICustomerService
    {
        Task<ServiceResponseDto> CustomerAddAsync(CustomerCreateDto customerCreateDto);
        ServiceResponseDto<List<CustomerDto>> GetCustomerAll();
        ServiceResponseDto<List<CustomerDto>> GetCustomerQuery(CustomerQueryDto customerQueryDto);
        Task<ServiceResponseDto<CustomerDto>> GetCustomerById(int id);
        Task<ServiceResponseDto> CustomerUpdateAsync(CustomerUpdateDto customerUpdateDto);
    }
}
