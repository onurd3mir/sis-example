﻿using AutoMapper;
using SIS.Application.Dtos;
using SIS.Application.Dtos.Customers;
using SIS.Application.Repositories.Customers;
using SIS.Application.Services.Interface;
using SIS.Domain.Entities;
using System.Linq.Expressions;

namespace SIS.Application.Services
{
    public class CustomerManager : ICustomerService
    {
        private readonly IMapper _mapper;
        private readonly ICustomerWriteRepository _customerWriteRepository;
        private readonly ICustomerReadRepositoy _customerReadRepositoy;

        public CustomerManager(IMapper mapper, ICustomerWriteRepository customerWriteRepository, ICustomerReadRepositoy customerReadRepositoy)
        {
            _mapper = mapper;
            _customerWriteRepository = customerWriteRepository;
            _customerReadRepositoy = customerReadRepositoy;
        }

        public async Task<ServiceResponseDto> CustomerAddAsync(CustomerCreateDto customerCreateDto)
        {
            var entity = _mapper.Map<Customer>(customerCreateDto);
            await _customerWriteRepository.AddAsync(entity);
            var result = await _customerWriteRepository.SaveAsync() == 1;

            return new ServiceResponseDto
            {
                Result = result,
                Message = result == true ? "Başarılı" : "Başarısız"
            };
        }

        public async Task<ServiceResponseDto> CustomerUpdateAsync(CustomerUpdateDto customerUpdateDto)
        {
            var entity = _mapper.Map<Customer>(customerUpdateDto);
            _customerWriteRepository.Update(entity);
            var result = await _customerWriteRepository.SaveAsync() == 1;

            return new ServiceResponseDto
            {
                Result = result,
                Message = result == true ? "Başarılı" : "Başarısız"
            };
        }

        public ServiceResponseDto<List<CustomerDto>> GetCustomerAll()
        {
            var result = _customerReadRepositoy.GetAll(true).ToList();

            return new ServiceResponseDto<List<CustomerDto>>()
            {
                Result = true,
                Data = _mapper.Map<List<CustomerDto>>(result),
                Message = "Başarılı"
            };
        }

        public async Task<ServiceResponseDto<CustomerDto>> GetCustomerById(int id)
        {
            var result = await _customerReadRepositoy.GetByIdAsync(id);

            return new ServiceResponseDto<CustomerDto>()
            {
                Result = true,
                Data = _mapper.Map<CustomerDto>(result),
                Message = "Başarılı"
            };
        }

        public ServiceResponseDto<List<CustomerDto>> GetCustomerQuery(CustomerQueryDto customerQueryDto)
        {
            Expression<Func<Customer, bool>> isMatch = x =>
                 (customerQueryDto.FullName == null || x.FullName.Contains(customerQueryDto.FullName)) &&
                 (customerQueryDto.IdentityNumber == null || x.IdentityNumber.Contains(customerQueryDto.IdentityNumber));

            var result = _customerReadRepositoy.GetWhere(isMatch, true).ToList();

            return new ServiceResponseDto<List<CustomerDto>>()
            {
                Result = true,
                Data = _mapper.Map<List<CustomerDto>>(result),
                Message = "Başarılı"
            };
        }
    }
}
