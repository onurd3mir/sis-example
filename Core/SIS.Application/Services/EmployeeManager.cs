﻿using AutoMapper;
using SIS.Application.Dtos;
using SIS.Application.Dtos.Employees;
using SIS.Application.Repositories.Employees;
using SIS.Application.Services.Interface;
using SIS.Domain.Entities;
using System.Linq.Expressions;

namespace SIS.Application.Services
{
    public class EmployeeManager : IEmployeeService
    {
        private readonly IMapper _mapper;
        private readonly IEmployeeReadRepository _employeeReadRepository;
        private readonly IEmployeeWriteRepository _employeeWriteRepository;

        public EmployeeManager(IEmployeeReadRepository employeeReadRepository, IEmployeeWriteRepository employeeWriteRepository, IMapper mapper)
        {
            _employeeReadRepository = employeeReadRepository;
            _employeeWriteRepository = employeeWriteRepository;
            _mapper = mapper;
        }

        public async Task<ServiceResponseDto> EmployeeAddAsync(EmployeeCreateDto employeeInputDto)
        {
            var entity = _mapper.Map<Employee>(employeeInputDto);
            await _employeeWriteRepository.AddAsync(entity);
            var result = await _employeeWriteRepository.SaveAsync() == 1;

            return new ServiceResponseDto
            {
                Result = result,
                Message = result == true ? "Başarılı" : "Bşarısız"
            };
        }

        public async Task<ServiceResponseDto<EmployeeDto>> EmployeeAuthorizeAsync(EmployeeAuthorizeDto employeeAuthorizeDto)
        {
            var result = await _employeeReadRepository.EmployeeAuthorizeAsync(employeeAuthorizeDto.Email, employeeAuthorizeDto.Password);

            return new ServiceResponseDto<EmployeeDto>
            {
                Data = _mapper.Map<EmployeeDto>(result),
                Result = true,
                Message = "Başarılı"
            };
        }

        public async Task<ServiceResponseDto> EmployeeUpdateAsync(EmployeeUpdateDto employeeUpdateDto)
        {
            var entity = _mapper.Map<Employee>(employeeUpdateDto);
            _employeeWriteRepository.Update(entity);
            var result = await _employeeWriteRepository.SaveAsync() == 1;

            return new ServiceResponseDto
            {
                Result = result,
                Message = result == true ? "Başarılı" : "Bşarısız"
            };
        }

        public ServiceResponseDto<List<EmployeeDto>> GetEmployeeAll()
        {
            var result = _employeeReadRepository.GetAll(tracking: true).ToList();

            return new ServiceResponseDto<List<EmployeeDto>>
            {
                Data = _mapper.Map<List<EmployeeDto>>(result),
                Result = true,
                Message = "Başarılı"
            };
        }

        public async Task<ServiceResponseDto<EmployeeDto>> GetEmployeeById(int id)
        {
            var result = await _employeeReadRepository.GetByIdAsync(id);

            return new ServiceResponseDto<EmployeeDto>
            {
                Data = _mapper.Map<EmployeeDto>(result),
                Result = true,
                Message = "Başarılı"
            };
        }

        public ServiceResponseDto<List<EmployeeDto>> GetEmployeeQuery(EmployeeQueryDto employeeQueryDto)
        {
            Expression<Func<Employee, bool>> isMatch = x =>
                 (employeeQueryDto.FullName == null || x.FullName.Contains(employeeQueryDto.FullName)) &&
                 (employeeQueryDto.EmployeeType == null || x.EmployeeType == employeeQueryDto.EmployeeType);

            var result = _employeeReadRepository.GetWhere(isMatch, true).ToList();

            return new ServiceResponseDto<List<EmployeeDto>>
            {
                Data = _mapper.Map<List<EmployeeDto>>(result),
                Result = true,
                Message = "Başarılı"
            };
        }
    }
}
