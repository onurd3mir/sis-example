﻿using AutoMapper;
using SIS.Application.Dtos.Customers;
using SIS.Application.Dtos.Employees;
using SIS.Application.Dtos.Sessions;
using SIS.Domain.Entities;

namespace SIS.Application.Mapping.AutoMapper
{
    public class ApplicationMappingProfile : Profile
    {
        public ApplicationMappingProfile()
        {
            #region Employee
            CreateMap<Employee, EmployeeCreateDto>().ReverseMap();
            CreateMap<Employee, EmployeeDto>()
                .ForMember(dest => dest.EmployeeId, opt => { opt.MapFrom(src => src.Id); }).ReverseMap();
            CreateMap<EmployeeUpdateDto, Employee>()
               .ForMember(dest => dest.Id, opt => { opt.MapFrom(src => src.EmployeeId); }).ReverseMap();
            #endregion

            #region Customer
            CreateMap<Customer, CustomerCreateDto>().ReverseMap();
            CreateMap<Customer, CustomerDto>()
               .ForMember(dest => dest.CustomerId, opt => { opt.MapFrom(src => src.Id); }).ReverseMap();
            CreateMap<CustomerUpdateDto, Customer>()
               .ForMember(dest => dest.Id, opt => { opt.MapFrom(src => src.CustomerId); }).ReverseMap();
            #endregion

            CreateMap<Session, SessionCreateDto>().ReverseMap();
        }
    }
}
