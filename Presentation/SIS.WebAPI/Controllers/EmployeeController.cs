﻿using Microsoft.AspNetCore.Mvc;
using SIS.Application.Dtos.Employees;
using SIS.Application.Services.Interface;

namespace SIS.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;

        public EmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        [HttpGet("GetEmployeeById")]
        public async Task<IActionResult> GetEmployeeById(int id)
        {
            return Ok(await _employeeService.GetEmployeeById(id));
        }

        [HttpGet("GetEmployeeList")]
        public IActionResult GetEmployeList()
        {
            return Ok(_employeeService.GetEmployeeAll());
        }

        [HttpPost("EmployeeAuthorize")]
        public async Task<IActionResult> EmployeeAuthorize(EmployeeAuthorizeDto employeeAuthorizeDto)
        {
            return Ok(await _employeeService.EmployeeAuthorizeAsync(employeeAuthorizeDto));
        }

        [HttpPost("GetEmployeeListQuery")]
        public IActionResult GetEmployeeListQuery(EmployeeQueryDto employeeQueryDto)
        {
            return Ok(_employeeService.GetEmployeeQuery(employeeQueryDto));
        }

        [HttpPost("SaveEmployee")]
        public async Task<IActionResult> SaveEmployee(EmployeeCreateDto employeeInputDto)
        {
            return Ok(await _employeeService.EmployeeAddAsync(employeeInputDto));
        }

        [HttpPut("EditEmployee")]
        public async Task<IActionResult> EditEmployee(EmployeeUpdateDto employeeUpdateDto)
        {
            return Ok(await _employeeService.EmployeeUpdateAsync(employeeUpdateDto));
        }
    }
}
