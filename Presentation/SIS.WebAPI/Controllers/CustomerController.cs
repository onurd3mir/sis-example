﻿using Microsoft.AspNetCore.Mvc;
using SIS.Application.Dtos.Customers;
using SIS.Application.Services.Interface;

namespace SIS.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerService _customerService;

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpGet("GetCustomerById")]
        public async Task<IActionResult> GetCustomerBydId(int id)
        {
            return Ok(await _customerService.GetCustomerById(id));
        }

        [HttpGet("GetCustomerList")]
        public IActionResult GetCustomerList()
        {
            return Ok(_customerService.GetCustomerAll());
        }

        [HttpPost("GetCustomerListQuery")]
        public IActionResult GetCustomerListQuery(CustomerQueryDto customerQueryDto)
        {
            return Ok(_customerService.GetCustomerQuery(customerQueryDto));
        }

        [HttpPost("SaveCustomer")]
        public async Task<IActionResult> SaveCustomer(CustomerCreateDto customerCreateDto)
        {
            return Ok(await _customerService.CustomerAddAsync(customerCreateDto));
        }

        [HttpPut("EditCustomer")]
        public async Task<IActionResult> EditCustomer(CustomerUpdateDto customerUpdateDto)
        {
            return Ok(await _customerService.CustomerUpdateAsync(customerUpdateDto));
        }
    }
}
