﻿using Microsoft.AspNetCore.Mvc;
using SIS.Application.Dtos.Sessions;
using SIS.Application.Services.Interface;

namespace SIS.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SessionController : ControllerBase
    {
        private readonly ISessionService _sessionService;

        public SessionController(ISessionService sessionService)
        {
            _sessionService = sessionService;
        }

        [HttpGet("GetCustomerEndMeetingInfo")]
        public IActionResult GetCustomerEndMeetingInfo(int customerId)
        {
            return Ok(_sessionService.GetCustomerEndMeetingInfo(customerId));
        }

        [HttpGet("GetCustomerEndSessionInfo")]
        public IActionResult GetCustomerEndSessionInfo(int customerId)
        {
            return Ok(_sessionService.GetCustomerEndSessionInfo(customerId));
        }

        [HttpGet("SessionMeetingCancel")]
        public async Task<IActionResult> SessionMeetingCancel(int id)
        {
            return Ok(await _sessionService.SessionMeetingCancelAsync(id));
        }

        [HttpPost("SaveSession")]
        public async Task<IActionResult> SaveSession(SessionCreateDto sessionCreateDto)
        {
            return Ok(await _sessionService.SessionAddAsync(sessionCreateDto));
        }

        [HttpPost("SessionListQuery")]
        public IActionResult SessionListQuery(SessionQueryDto sessionQueryDto)
        {
            return Ok(_sessionService.GetSesionQuery(sessionQueryDto));
        }

        [HttpPost("ExistSessionCount")]
        public IActionResult ExistSessionCount(SessionQueryDto sessionQueryDto)
        {
            return Ok(_sessionService.ExistSessionCount(sessionQueryDto));
        }

        [HttpPost("SessionNoteUpdate")]
        public async Task<IActionResult> SessionNoteUpdate(SessionNoteUpdateDto sessionNoteUpdateDto)
        {
            return Ok(await _sessionService.SessionNoteUpdateAsync(sessionNoteUpdateDto));
        }

        [HttpPost("SessionMeetingSave")]
        public async Task<IActionResult> SessionMeetingSave(SessionMeetingSaveDto sessionMeetingSaveDto)
        {
            return Ok(await _sessionService.SessionMeetingSaveAsync(sessionMeetingSaveDto));
        }

        [HttpPost("SessionCancel")]
        public async Task<IActionResult> SessionCancel(SessionCancelDto sessionCancelDto)
        {
            return Ok(await _sessionService.SessionCancelAsync(sessionCancelDto));
        }
    }
}
