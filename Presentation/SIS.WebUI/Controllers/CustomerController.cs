﻿using Microsoft.AspNetCore.Mvc;
using SIS.Application.Dtos.Customers;
using SIS.Application.Services.Interface;

namespace SIS.WebUI.Controllers
{
    public class CustomerController : Controller
    {
        private readonly ICustomerService _customerService;

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CustomerListQuery(CustomerQueryDto customerQueryDto)
        {
            return Json(_customerService.GetCustomerQuery(customerQueryDto));
        }
    }
}
