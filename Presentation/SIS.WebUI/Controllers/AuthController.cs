﻿using Microsoft.AspNetCore.Mvc;
using SIS.Application.Dtos.Employees;
using SIS.Application.Services.Interface;

namespace SIS.WebUI.Controllers
{
    public class AuthController : Controller
    {
        private readonly IEmployeeService _employeeService;

        public AuthController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        public IActionResult Index()
        {
            return View(new EmployeeAuthorizeDto());
        }

        [HttpPost]
        public async Task<IActionResult> Index(EmployeeAuthorizeDto model)
        {
            var result = await _employeeService.EmployeeAuthorizeAsync(model);

            if (result.Data == null)
            {
                return View(model);
            }

            return RedirectToAction("Index","Home"); 
        }

    }
}
