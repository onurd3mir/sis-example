﻿using Microsoft.EntityFrameworkCore;
using SIS.Domain.Entities;
using SIS.Domain.Entities.Common;
using SIS.Persistence.Mapping.Mysql;

namespace SIS.Persistence.Context
{
    public class SisDbContext : DbContext
    {
        public SisDbContext() { }

        public SisDbContext(DbContextOptions<SisDbContext> options)
            : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Session> Sessions { get; set; }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            var datas = ChangeTracker.Entries<BaseEntity>();

            foreach (var data in datas)
            {
                _ = data.State switch
                {
                    EntityState.Added => data.Entity.CreateDate = DateTime.Now.Date,
                    EntityState.Modified => data.Entity.CreateDate,
                };
            }

            return await base.SaveChangesAsync(cancellationToken);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new MysqlCustomerEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new MysqlEmployeeEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new MysqlSessionEntityTypeConfiguration());
        }
    }
}
