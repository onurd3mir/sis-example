﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SIS.Application.Repositories.Customers;
using SIS.Application.Repositories.Employees;
using SIS.Application.Repositories.Sessions;
using SIS.Persistence.Context;
using SIS.Persistence.Respositories.Cutomers;
using SIS.Persistence.Respositories.Employees;
using SIS.Persistence.Respositories.Sessions;

namespace SIS.Persistence.Extensions
{
    public static class ServiceRegistration
    {
        public static void LoadPersistenceServices(this IServiceCollection services)
        {
            ConfigurationManager configurationManager = new();
            configurationManager.SetBasePath(Path.Combine(Directory.GetCurrentDirectory(), "../../Presentation/SIS.WebAPI"));
            configurationManager.AddJsonFile("appsettings.json");

            services.AddDbContext<SisDbContext>(options =>
                    options.UseMySQL(configurationManager.GetConnectionString("DefaultConnection")));

            services.AddScoped<ICustomerReadRepositoy, CustomerReadRepository>();
            services.AddScoped<ICustomerWriteRepository, CustomerWriteRepository>();
            services.AddScoped<IEmployeeReadRepository, EmployeeReadRepository>();
            services.AddScoped<IEmployeeWriteRepository, EmployeeWriteRepository>();
            services.AddScoped<ISessionReadRepository, SessionReadRepository>();
            services.AddScoped<ISessionWriteRepository, SessionWriteRepository>();
        }
    }
}
