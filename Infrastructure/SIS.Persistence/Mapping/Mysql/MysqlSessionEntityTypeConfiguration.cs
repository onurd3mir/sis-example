﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SIS.Domain.Entities;

namespace SIS.Persistence.Mapping.Mysql
{
    internal class MysqlSessionEntityTypeConfiguration : IEntityTypeConfiguration<Session>
    {
        public void Configure(EntityTypeBuilder<Session> builder)
        {
            builder.ToTable("Session");

            builder.Property(x => x.Id)
               .HasColumnName("SessionId")
               .IsRequired();

            builder.HasOne(c => c.Customer)
                .WithMany(s => s.Sessions)
                .HasForeignKey(c => c.CustomerId);

            builder.HasOne(e => e.Employee)
               .WithMany(s => s.Sessions)
               .HasForeignKey(e => e.ExpertEmployeeId);

            builder.HasOne(e => e.Employee)
              .WithMany(s => s.Sessions)
              .HasForeignKey(e => e.CancelEmployeeId);
        }
    }
}
