﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SIS.Domain.Entities;

namespace SIS.Persistence.Mapping.Mysql
{
    public class MysqlCustomerEntityTypeConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.ToTable("Customer");

            builder.Property(x => x.Id)
                .HasColumnName("CustomerId")
                .IsRequired();

            //builder.HasMany(d => d.Sessions)
            //    .WithOne(e => e.Customer)
            //    .HasForeignKey(e => e.CustomerId);
        }
    }
}
