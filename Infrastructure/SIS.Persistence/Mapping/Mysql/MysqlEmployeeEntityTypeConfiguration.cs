﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SIS.Domain.Entities;

namespace SIS.Persistence.Mapping.Mysql
{
    internal class MysqlEmployeeEntityTypeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.ToTable("Employee");

            builder.Property(x => x.Id)
              .HasColumnName("EmployeeId")
              .IsRequired();

            //builder.HasMany(s => s.Sessions)
            //    .WithOne(e => e.Employee)
            //    .HasForeignKey(e => e.ExpertEmployeeId);

            //builder.HasMany(s => s.Sessions)
            //    .WithOne(e => e.Employee)
            //    .HasForeignKey(e => e.CancelEmployeeId);
        }
    }
}
