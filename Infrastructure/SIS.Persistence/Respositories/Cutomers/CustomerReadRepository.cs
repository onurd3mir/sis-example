﻿using SIS.Application.Repositories.Customers;
using SIS.Domain.Entities;
using SIS.Persistence.Context;

namespace SIS.Persistence.Respositories.Cutomers
{
    public class CustomerReadRepository : ReadRepository<Customer>, ICustomerReadRepositoy
    {
        public CustomerReadRepository(SisDbContext context)
            : base(context)
        {

        }
    }
}
