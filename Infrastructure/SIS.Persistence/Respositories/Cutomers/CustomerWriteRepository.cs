﻿using SIS.Application.Repositories.Customers;
using SIS.Domain.Entities;
using SIS.Persistence.Context;

namespace SIS.Persistence.Respositories.Cutomers
{
    public class CustomerWriteRepository : WriteRepository<Customer>, ICustomerWriteRepository
    {
        public CustomerWriteRepository(SisDbContext context)
            : base(context)
        {

        }
    }
}
