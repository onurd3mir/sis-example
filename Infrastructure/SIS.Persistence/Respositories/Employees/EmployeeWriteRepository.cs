﻿using SIS.Application.Repositories.Employees;
using SIS.Domain.Entities;
using SIS.Persistence.Context;

namespace SIS.Persistence.Respositories.Employees
{
    public class EmployeeWriteRepository :WriteRepository<Employee>, IEmployeeWriteRepository
    {
        public EmployeeWriteRepository(SisDbContext context)
            : base(context)
        {

        }
    }
}
