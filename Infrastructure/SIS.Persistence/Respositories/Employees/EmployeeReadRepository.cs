﻿using Microsoft.EntityFrameworkCore;
using SIS.Application.Repositories.Employees;
using SIS.Domain.Entities;
using SIS.Persistence.Context;

namespace SIS.Persistence.Respositories.Employees
{
    public class EmployeeReadRepository : ReadRepository<Employee>, IEmployeeReadRepository
    {
        private readonly SisDbContext _context;

        public EmployeeReadRepository(SisDbContext context)
            : base(context)
        {
            _context = context;
        }

        public async Task<Employee> EmployeeAuthorizeAsync(string email, string password)
        {
            return await _context.Employees.FirstOrDefaultAsync(x => x.Email == email && x.Password == password);
        }
    }
}
