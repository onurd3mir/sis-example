﻿using SIS.Application.Repositories.Sessions;
using SIS.Domain.Entities;
using SIS.Persistence.Context;

namespace SIS.Persistence.Respositories.Sessions
{
    public class SessionWriteRepository : WriteRepository<Session>, ISessionWriteRepository
    {
        public SessionWriteRepository(SisDbContext context)
            : base(context)
        {

        }
    }
}
