﻿using Microsoft.EntityFrameworkCore;
using SIS.Application.Dtos.Sessions;
using SIS.Application.Repositories.Sessions;
using SIS.Domain.Entities;
using SIS.Persistence.Context;

namespace SIS.Persistence.Respositories.Sessions
{
    public class SessionReadRepository : ReadRepository<Session>, ISessionReadRepository
    {
        private readonly SisDbContext _context;
        public SessionReadRepository(SisDbContext context)
            : base(context)
        {
            _context = context;
        }

        public SessionDto GetCustomerEndMeetingInfo(int customerId)
        {
            using var command = _context.Database.GetDbConnection().CreateCommand();
            command.CommandText = $"SP_GetCustomerEndMeetingInfo({customerId})";
            _context.Database.OpenConnection();
            using var result = command.ExecuteReader();
            result.Read();
            var model = new SessionDto
            {
                SessionId = result.GetInt32(0),
                CustomerId = result.GetInt32(1),
                CustomerFullName = result.GetString(2),
                ExpertEmployeeId = result.GetInt32(3),
                ExpertEmployeFullName = result.GetString(4),
                CreateDate = result.GetDateTime(5),
                StartDate = result.GetString(6),
                EndDate = result.GetString(7),
                SessionNote = result.IsDBNull(8) ? "" : result.GetString(8),
                CancelNote = result.IsDBNull(9) ? "" : result.GetString(9),
                SessionStatus = result.GetBoolean(10)
            };
            return model;
        }

        public SessionDto GetCustomerEndSessionInfo(int customerId)
        {
            var result = (from session in _context.Sessions
                          from employee in _context.Employees.Where(x => x.Id == session.ExpertEmployeeId).DefaultIfEmpty()
                          from customer in _context.Customers.Where(x => x.Id == session.CustomerId).DefaultIfEmpty()
                          where session.CustomerId == customerId && session.SessionStatus == true
                          select new SessionDto
                          {
                              SessionId = session.Id,
                              CustomerFullName = customer.FullName,
                              ExpertEmployeFullName = employee.FullName,
                              CreateDate = session.CreateDate,
                              StartDate = session.StartDate,
                              EndDate = session.EndDate,
                              SessionNote = session.SessionNote,
                              SessionStatus = session.SessionStatus,
                              CancelNote = session.CancelNote,
                              ExpertEmployeeId = employee.Id,
                              CustomerId = customer.Id,
                          })
                          .AsNoTracking()
                          .OrderByDescending(x => x.CreateDate)
                          .FirstOrDefault();
            return result;
        }

        /// <summary>
        /// Müşteri veya Personel Seans Kayıtları Sorgular
        /// </summary>
        /// <param name="sessionQueryDto"></param>
        /// <returns></returns>
        public List<SessionDto> GetSesionQuery(SessionQueryDto sessionQueryDto)
        {
            var result = (from session in _context.Sessions
                          from customer in _context.Customers.Where(c => c.Id == session.CustomerId).DefaultIfEmpty()
                          from employee in _context.Employees.Where(e => e.Id == session.ExpertEmployeeId).DefaultIfEmpty()
                          where session.CancelEmployeeId.Equals(null) &&
                          (sessionQueryDto.CustomerId == null || session.CustomerId == sessionQueryDto.CustomerId) &&
                          (sessionQueryDto.ExpertEmployeeId == null || session.ExpertEmployeeId == sessionQueryDto.ExpertEmployeeId) &&
                          (session.SessionStatus == sessionQueryDto.SessionStatus)
                          select new SessionDto
                          {
                              SessionId = session.Id,
                              CustomerFullName = customer.FullName,
                              ExpertEmployeFullName = employee.FullName,
                              CreateDate = session.CreateDate,
                              StartDate = session.StartDate,
                              EndDate = session.EndDate,
                              SessionNote = session.SessionNote,
                              SessionStatus = session.SessionStatus,
                              CancelNote = session.CancelNote,
                              ExpertEmployeeId = employee.Id,
                              CustomerId = customer.Id,
                          })
                          .AsNoTracking()
                          .OrderByDescending(s => s.CreateDate)
                          .ThenByDescending(s => s.StartDate)
                          .ToList();

            return result;
        }
    }
}
